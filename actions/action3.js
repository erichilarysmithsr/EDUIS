"use strict";
let datafire = require('datafire');

let google_youtubeanalytics = require('@datafire/google_youtubeanalytics').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let resultTable = await Promise.all([].map(item => google_youtubeanalytics.reports.query({
      ids: "",
      'start-date': "",
      'end-date': "",
      metrics: "",
    }, context)));
    return resultTable;
  },
});
