"use strict";
let datafire = require('datafire');

let google_blogger = require('@datafire/google_blogger').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let blog = await Promise.all([].map(item => google_blogger.blogs.get({
      blogId: "",
    }, context)));
    return blog;
  },
});
