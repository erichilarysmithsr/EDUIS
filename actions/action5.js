"use strict";
let datafire = require('datafire');

let swaggerui = require('@datafire/swaggerui').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let result = await Promise.all([].map(item => swaggerui.get({}, context)));
    return result;
  },
});
